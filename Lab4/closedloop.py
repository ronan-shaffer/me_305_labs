''' @file                       closedloop.py
    @brief                      A driver for closed loop control
    @details                    Performs speed control for the motor driver.
    @author                     Ronan Shaffer
    @author                     Nishka Chawla
    @date                       10/29/2021
'''

class ClosedLoop:
    ''' @brief                  Closed-loop controller class
        @details                Class containing set_Kp, run, and get_Kp methods 
                                to perform speed control for the motor driver.
    '''
    
    def __init__(self, ref, msr, sat_max, sat_min):
        ''' @brief
            @details
            @param ref The reference value.
            @param msr The measured value.
            @param sat_max The maximum saturation limit on the PWM level.
            @param sat_min The minimum saturation limit on the PWM level.
        '''
        
        ## @brief   Initialisation of error variable
        self.error = 0
        
        ## Initialisation of reference variable
        self.ref = ref
        
        ## Initialisation of measured variable
        self.msr = msr
        
        ## Saturation High Limit
        self.sat_max = sat_max
        
        ## Saturation Low Limit
        self.sat_min = sat_min
        
        ## @brief   Initialisation of actuation signal variable.
        self.L = 0
        
    def set_Kp(self, Kp):
        ''' @brief              Sets the proportional gain value.  
            @details            Sets the Kp gain in the closedloop driver from 
                                the shared gains variable written in the user task.
            @param  Kp          Proportional gain value set by the user.
        '''
        self.Kp = Kp

    def run(self):
        ''' @brief              Performs control on the motor speed.
            @details            Computes and returns the actuation value based 
                                on the measured and reference values.
            @return             The actuation signal after computing the 
                                controller output.
        '''        
#        add integral stuff if we want
        self.error = self.ref - self.msr
        
        if self.ref > 0:
            self.L = self.Kp*abs(self.error)
        if self.ref < 0:
            self.L = self.Kp*abs(self.error)*-1
        
        if self.L > self.sat_max:
            self.L = self.sat_max
        if self.L < self.sat_min:
            self.L = self.sat_min
        return self.L
    

    def get_Kp(self):
        ''' @brief              Returns the set proportional gain value.
            @details            Returns the proportional gain value set in the 
                                set_Kp function.
            @return             Set proportional gain value.
        '''  
        return self.Kp


#if __name__ == '__main__':
#        controller1 = ClosedLoop(50, 0, 200, -200)
#        controller2 = ClosedLoop(50, 0, 200, -200)
#        controller1.set_Kp(1)
#        controller1.run()
