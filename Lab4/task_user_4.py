''' @file                       task_user_4.py
    @brief                      Interfaces with program user.
    @details                    Receives user input and communicates with 
                                task_encoder to print encoder information 
                                corresponding to relevant user command.
    @author                     Nishka Chawla
    @author                     Ronan Shaffer
    @date                       10/19/2021
'''

import utime, pyb
from array import array
import shares

## State 0 of the user interface task
_S0_INIT        = 0
## State 1 of the user interface task
_S1_WAIT        = 1
## State 2 of the user interface task
_S2_COLLECT_G1  = 2
## State 3 of the user interface task
_S3_PRINT_G1    = 3
## State 4 of the user interface task
_S4_COLLECT_G2  = 4
## State 5 of the user interface task
_S5_PRINT_G2    = 5
## State 6 of the user interface task
_S6_ZERO_1      = 6
## State 7 of the user interface task
_S7_ZERO_2      = 7
## State 8 of the user interface task
_S8_PRINT_1     = 8
## State 9 of the user interface task
_S9_PRINT_2     = 9
## State 10 of the user interface task
_S10_DELTA_1    = 10
## State 11 of the user interface task
_S11_DELTA_2    = 11
## State 12 of the user interface task
_S12_DUTY_1     = 12
## State 13 of the user interface task
_S13_DUTY_2     = 13
## State 14 of the user interface task
_S14_FAULT_WAIT = 14
## State 15 of the user interface task
_S15_FAULT_CLEAR= 15
## State 16 of the user interface task
_S16_ENABLE     = 16
## State 17 of the user interface task
_S17_GAIN_1     = 17
## State 18 of the user interface task
_S18_GAIN_2     = 18
## State 19 of the user interface task
_S19_VELO_1     = 19
## State 20 of the user interface task
_S20_VELO_2     = 20
## State 21 of the user interface task
_S21_STP_RES_1  = 21
## State 22 of the user interface task
_S22_STP_RES_2  = 22

class Task_User():
    ''' @brief      User interface task for cooperative multitasking.
        @details    Implements a finite state machine to take user input
                    commands and communicate with encoder task via shares.
    '''
    
    def __init__(self, period, z_flags, enable_flag, fault_user_flag, positions, deltas, speeds, gains, encoders, motors, Ls):
        ''' @brief                  Communicates with other tasks.
            @details                Accepts user input and communicates with 
                                    task_encoder and task_motor.
            @param period           The period, in microseconds, between runs 
                                    of the task.
            @param z_flags          Boolean flags used to instruct the encoder 
                                    task to set the most recent encoder position 
                                    to zero.
            @param enable_flag      A boolean flag used to instruct the motor
                                    task to enable the motors from user input.
            @param fault_user_flag  A boolean flag used to inform the user of 
                                    a fault detection. Flag turned down when 
                                    cleared by the user.
            @param positions        The most recent orientation read by the 
                                    encoders.
            @param deltas           The number of ticks between the two most 
                                    recent positions recorded by the encoders.
            @param speeds           The velocity setpoint of the motors, in rad/s.
            @param gains            The proportional gain set by the user in the 
                                    user task.
            @param encoders         Encoder objects in the task_encoder class.
            @param motors           Object containing an index of motor objects.
            @param Ls               Sets the duty cycle of the motors from the 
                                    actuation level determined in the closedloop 
                                    driver.
        '''
        self._n = 0
        
        self._ser = pyb.USB_VCP()
        
        ## @brief   The period (in us) between task iterations.
        #  @details Time period that must elapse since last iteration before
        #           task runs again.
        self.period = period
        
        ## @brief   A flag that instructs encoder task to zero current position.
        #  @details Binary flag that signals to encoder task to run the
        #           set_position method.
        self.z_flags = z_flags
        
        ## @brief   Stores current position of the encoders.
        self.positions = positions
        
        ## @brief   Encoder object with attributes from Encoder class.
        self.encoders = encoders
        
        ## @brief   Stores difference between two most recent encoder positions.
        self.deltas = deltas
        
        ## @brief   Stores the velocity setpoint of the motors.
        self.speeds = speeds
        
        ## @brief   Stores the proportional gains for the motors.
        self.gains = gains
        
        self.array_size = int((10000000 /self.period.read()) + 2)
        
        ## @brief   List storing position data from encoder.
        self.pos_list = array("f", [0] * self.array_size)
        
        ## @brief   List storing delta data from encoder timer.
        self.delta_list = array("f", [0] * self.array_size)

        ## @brief   List storing time data from encoder timer.
        self.time_list = array("f", [0] * self.array_size)

        ## @brief   List storing delta data from encoder timer.
        self.meas_list = array("f", [0] * self.array_size)
        
        ## @brief   List storing the velocity setpoint.
        self.ref_list = array("f", [0] * self.array_size)
        
        ## @brief   List storing the actuation level data from the closedloop controller.
        self.act_list = array("f", [0] * self.array_size)

        ## The state to run on the next iteration of the finite state machine
        self._state = _S0_INIT
        
        ## The number of runs of the state machine
        self._runs = 0
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self._next_time = utime.ticks_add(utime.ticks_us(), self.period.read())
        
        self.enable_flag = enable_flag
        
        self.fault_user_flag = fault_user_flag
        
        self.Ls = Ls
        
        
    def run(self):
        ''' @brief Runs one iteration of the FSM
        '''
        _current_time = utime.ticks_us()
        if (utime.ticks_diff(_current_time, self._next_time) >= 0):
            if self._state == _S0_INIT:
                print('Welcome!')
                print('Press \'z\' to zero the position of Encoder 1')
                print('Press \'Z\' to zero the position of Encoder 2')
                print('Press \'p\' to print out the position of Encoder 1')
                print('Press \'P\' to print out the position of Encoder 2')
                print('Press \'d\' to print out the speed of Motor 1')
                print('Press \'D\' to print out the speed of Motor 2')
                print('Press \'m\' to enter a speed for Motor 1')
                print('Press \'M\' to enter a speed for Motor 2')
#                print('Press \'k\' to set a gain for Motor 1')
                print('Press \'c\' to clear a fault')
                print('Press \'g\' to collect Encoder 1 data for 30 seconds')
                print('Press \'G\' to collect Encoder 2 data for 30 seconds')
                print('Press \'s\' to end data collection prematurely.')
                print('Press \'e\' to enable and disable the motors.')
                print('Press \'1\' to perform a step response on Motor 1.')
                print('Press \'2\' to perform a step response on Motor 2.')
                self.transition_to(_S1_WAIT)
                
            elif self._state == _S1_WAIT:
                
                if self.fault_user_flag.read() == 1:
                    self.transition_to(_S14_FAULT_WAIT)
                
                if self._ser.any():
                    ## @brief   Stores user input command as a string
                    char_in = self._ser.read(1).decode()
                    
                    if(char_in == 'z'):
                        self.transition_to(_S6_ZERO_1)
                        
                    elif(char_in == 'Z'):
                        self.transition_to(_S7_ZERO_2)
                        
                    elif(char_in == 'p'):
                        self.transition_to(_S8_PRINT_1)
                        
                    elif(char_in == 'P'):
                        self.transition_to(_S9_PRINT_2)
                        
                    elif(char_in == 'd'):
                        self.transition_to(_S10_DELTA_1)
                        
                    elif(char_in == 'D'):
                        self.transition_to(_S11_DELTA_2)
                        
                    elif(char_in == 'g'):
                        print('Collecting Motor 1 data')
                        self._g_time = _current_time
                        self._idx = 0
                        self.transition_to(_S2_COLLECT_G1)
                        
                    elif(char_in == 'G'):
                        print('Collecting Motor 2 data')
                        self._g_time = _current_time
                        self._idx = 0
                        self.transition_to(_S4_COLLECT_G2)

                    elif(char_in == 's' or char_in == 'S'):
                        print('Data collection cancelled')
                        
                    elif(char_in == 'm'):
                        print('Choose a speed for Motor 1')
                        self.transition_to(_S12_DUTY_1)
                        self._inputs = ''
                        
                    elif(char_in == 'M'):
                        print('Choose a speed for Motor 2')
                        self.transition_to(_S13_DUTY_2)
                        self._inputs = ''
                        
                    elif(char_in == 'c' or char_in == 'C'):
                        self.transition_to(_S15_FAULT_CLEAR)
                        
                    elif(char_in == 'e' or char_in == 'E'):
                        self.transition_to(_S16_ENABLE)
                        
#                    elif(char_in == 'k'):
#                        print('Enter a proportional gain for Motor 1.')
#                        self.transition_to(_S17_GAIN_1)
#                        self._inputs = ''
                        
                    elif(char_in == '1'):
                        self.transition_to(_S17_GAIN_1)
#                        print('Performing a step response on Motor 1...')
                        print('Enter a proportional gain for Motor 1.')
                        self._inputs = ''
                        
                    elif(char_in == '2'):
                        self.transition_to(_S18_GAIN_2)
                        print('Enter a proportional gain for Motor 2.')
                        self._inputs = ''
                        
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_in))
                        
            
            elif self._state == _S2_COLLECT_G1:
                
                self.pos_list[self._idx] = self.positions[0].read()
                self.delta_list[self._idx] = self.deltas[0].read()
                self.time_list[self._idx] = utime.ticks_diff(_current_time, self._g_time)
                self._idx += 1

                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if(char_in == 's' or char_in == 'S'):
                        
                        print('Data collection cancelled')
                        self.transition_to(_S3_PRINT_G1)
                        
                if (utime.ticks_diff(_current_time, self._g_time) >= 10000000):
                    
                    self._g_time = 0
                    self.transition_to(_S3_PRINT_G1)

            elif self._state == _S3_PRINT_G1:
                    
                print('{:},{:},{:}'.format(self.time_list[self._n], self.pos_list[self._n], self.delta_list[self._n]))
                self._n += 1
                
                if self._n == self._idx:
                    
                    print('Done printing.')
                    self._n = 0
                    self.transition_to(_S1_WAIT)
                        
            elif self._state == _S4_COLLECT_G2:
                
                self.pos_list[self._idx] = self.positions[1].read()
                self.delta_list[self._idx] = self.deltas[1].read()
                self.time_list[self._idx] = utime.ticks_diff(_current_time, self._g_time)
                self._idx += 1

                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if(char_in == 's' or char_in == 'S'):
                        
                        print('Data collection cancelled')
                        self.transition_to(_S5_PRINT_G2)
                        
                if (utime.ticks_diff(_current_time, self._g_time) >= 10000000):
                    
                    self._g_time = 0
                    self.transition_to(_S5_PRINT_G2)
                
            elif self._state == _S5_PRINT_G2:
 
                print('{:},{:},{:}'.format(self.time_list[self._n], self.pos_list[self._n], self.delta_list[self._n]))
                self._n += 1
                
                if self._n == self._idx:
                    
                    print('Done printing.')
                    self._n = 0
                    self.transition_to(_S1_WAIT)
                
            elif self._state == _S6_ZERO_1:
                self.z_flags[0].write(1)
                print('Zeroing position of Encoder 1')
                self.transition_to(_S1_WAIT)
                
            elif self._state == _S7_ZERO_2:
                self.z_flags[1].write(1)
                print('Zeroing position of Encoder 2')
                self.transition_to(_S1_WAIT)
                
            elif self._state == _S8_PRINT_1:
                print('Encoder 1 position =', self.positions[0].read(), 'radians')
                self.transition_to(_S1_WAIT)
                
            elif self._state == _S9_PRINT_2:
                print('Encoder 2 position =', self.positions[1].read(), 'radians')
                self.transition_to(_S1_WAIT)
                
            elif self._state == _S10_DELTA_1:
                print('Motor 1 speed =', self.deltas[0].read(), 'rad/s')
                self.transition_to(_S1_WAIT)
                
            elif self._state == _S11_DELTA_2:
                print('Motor 2 speed =', self.deltas[1].read(), 'rad/s')
                self.transition_to(_S1_WAIT)
                
            elif self._state == _S12_DUTY_1:
                
                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r' or char_in == '\n':
                        
                        if len(self._inputs) == 0:
                            self.transition_to(_S1_WAIT)
                        
                        else:
                
                            if float(self._inputs) < -200:
                                self.speeds[0].write(-200)
                                
                            elif float(self._inputs) > 200:
                                self.speeds[0].write(200)
                                
                            elif float(self._inputs) <= 200 and float(self._inputs) >= -200:
                                self.speeds[0].write(float(self._inputs))
                            
                            self._ser.write('\r')
                            print('Chosen speed for Motor 1 is', self.speeds[0].read(), 'rad/s')
                            self.transition_to(_S1_WAIT)
                        
                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                
            elif self._state == _S13_DUTY_2:
                
                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r':
                
                        if float(self._inputs) < -200:
                            self.speeds[1].write(-200)
                            
                        if float(self._inputs) > 200:
                            self.speeds[1].write(200)
                            
                        if float(self._inputs) <= 200 and float(self._inputs) >= -200:
                            self.speeds[1].write(float(self._inputs))
                        
                        self._ser.write('\r')
                        print('Chosen speed for Motor 2 is', self.speeds[1].read(), 'rad/s')
                        self.transition_to(_S1_WAIT)

                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                        
            elif self._state == _S16_ENABLE:
                
                if (self.enable_flag.read() == 0):
                    self.enable_flag.write(1)
                    self.transition_to(_S1_WAIT)
                    print('Motors enabled')
                
                elif (self.enable_flag.read() == 1):
                    self.enable_flag.write(0)
                    self.transition_to(_S1_WAIT)
                    print('Motors disabled')
                
            elif self._state == _S14_FAULT_WAIT:
                print('Fault detected. Press c to clear fault.')
                self.fault_user_flag.write(2)
                self.transition_to(_S1_WAIT)
                
            elif self._state == _S15_FAULT_CLEAR:
                print('Fault cleared. Press e to enable motors.')
                self.fault_user_flag.write(0)
                self.transition_to(_S1_WAIT)
                
            elif self._state == _S17_GAIN_1:
                
                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r' or char_in == '\n':
                        
                        if len(self._inputs) == 0:
                            self.transition_to(_S1_WAIT)
                        
                        else:
                            self.gains[0].write(float(self._inputs))
                            self._ser.write('\r')
                            print('Chosen gain for Motor 1 is', self.gains[0].read(), '%/rad/s')
                            print('Enter a velocity setpoint for Motor 1')
                            self._inputs = ''
                            self.transition_to(_S19_VELO_1)
                        
                        
                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                            
            elif self._state == _S18_GAIN_2:
                
                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r' or char_in == '\n':
                        
                        if len(self._inputs) == 0:
                            self.transition_to(_S1_WAIT)
                        
                        else:
                            self.gains[1].write(float(self._inputs))
                            self._ser.write('\r')
                            print('Chosen gain for Motor 2 is', self.gains[1].read(), '%/rad/s')
                            print('Enter a velocity setpoint for Motor 2')
                            self._inputs = ''
                            self.transition_to(_S20_VELO_2)
                        
                        
                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                            
            elif self._state == _S19_VELO_1:
                
                if self._ser.any():
                
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r' or char_in == '\n':
                        
                        if len(self._inputs) == 0:
                            self.transition_to(_S1_WAIT)
                        
                        else:
                
                            if float(self._inputs) < -200:
                                self.speeds[0].write(-200)
                                
                            elif float(self._inputs) > 200:
                                self.speeds[0].write(200)
                                
                            elif float(self._inputs) <= 200 and float(self._inputs) >= -200:
                                self.speeds[0].write(float(self._inputs))
                            
                            self._ser.write('\r')
                            print('Chosen speed for Motor 1 is', self.speeds[0].read(), 'rad/s')
                            print('Generating step response...')
                            self.transition_to(_S21_STP_RES_1)
                            self._res_time = _current_time
                            self._idx = 0
                            self._s = 0
                            self.enable_flag.write(1)
                            
                        
                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                            
            elif self._state == _S20_VELO_2:
                
                if self._ser.any():
                
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r' or char_in == '\n':
                        
                        if len(self._inputs) == 0:
                            self.transition_to(_S1_WAIT)
                        
                        else:
                
                            if float(self._inputs) < -200:
                                self.speeds[1].write(-200)
                                
                            elif float(self._inputs) > 200:
                                self.speeds[1].write(200)
                                
                            elif float(self._inputs) <= 200 and float(self._inputs) >= -200:
                                self.speeds[1].write(float(self._inputs))
                            
                            self._ser.write('\r')
                            print('Chosen speed for Motor 2 is', self.speeds[1].read(), 'rad/s')
                            print('Generating step response...')
                            self.transition_to(_S22_STP_RES_2)
                            self._res_time = _current_time
                            self._idx = 0
                            self._s = 0
                            self.enable_flag.write(1)
                            
                        
                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                
            elif self._state == _S21_STP_RES_1:
                
                self.meas_list[self._idx] = self.deltas[0].read()
                self.ref_list[self._idx] = self.speeds[0].read()
                self.act_list[self._idx] = self.Ls[0].read()
                self.time_list[self._idx] = utime.ticks_diff(_current_time, self._res_time) / 1000000
                
                if self._ser.any():
                        
                    char_in = self._ser.read(1).decode()
                    
                    if(char_in == 's' or char_in == 'S'):
                        
                        print('Data collection cancelled')
                        self._s = 1
                            
                
                if self._idx == int(self.array_size-1) or self._s == 1:
                    
                    self.enable_flag.write(0)
                    self._res_time = 0
                    print('{:},{:},{:},{:}'.format(self.time_list[self._n], self.ref_list[self._n], self.meas_list[self._n], self.act_list[self._n]))
                    self._n += 1
                    
                    if self._n == self._idx:
                        
                        print('Done printing.')
                        self._n = 0
                        self.transition_to(_S1_WAIT)
                    
                else:
                    
                    self._idx += 1
                          
            elif self._state == _S22_STP_RES_2:
                
                self.meas_list[self._idx] = self.deltas[1].read()
                self.ref_list[self._idx] = self.speeds[1].read()
                self.act_list[self._idx] = self.Ls[1].read()
                self.time_list[self._idx] = utime.ticks_diff(_current_time, self._res_time) / 1000000
                
                if self._ser.any():
                        
                    char_in = self._ser.read(1).decode()
                    
                    if(char_in == 's' or char_in == 'S'):
                        
                        print('Data collection cancelled')
                        self._s = 1
                            
                
                if self._idx == int(self.array_size-1) or self._s == 1:
                    
                    self.enable_flag.write(0)
                    self._res_time = 0
                    print('{:},{:},{:},{:}'.format(self.time_list[self._n], self.ref_list[self._n], self.meas_list[self._n], self.act_list[self._n]))
                    self._n += 1
                    
                    if self._n == self._idx:
                        
                        print('Done printing.')
                        self._n = 0
                        self.transition_to(_S1_WAIT)
                    
                else:
                    
                    self._idx += 1
                            

            else:
                raise ValueError('Invalid State')
            
            self._next_time = utime.ticks_add(self._next_time, self.period.read())
            self._runs += 1
    
    def transition_to(self, _new_state):
        ''' @brief      Transitions the FSM to a new state
            @param      new_state The state to transition to next.
        '''
        self._state = _new_state