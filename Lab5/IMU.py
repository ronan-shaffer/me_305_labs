''' @file       IMU.py
    @brief      A driver for reading from the Inertial Measurement Unit
    @details
    @author     Nishka Chawla
    @author     Ronan Shaffer
    @date       11/3/2021
'''

from pyb import I2C
import struct
import time

CONFIGMODE  = 0
ACCONLY     = 1
MAGONLY     = 2
GYROONLY    = 3
ACCMAG      = 4
ACCGYRO     = 5
MAGGYRO     = 6
AMG         = 7
IMU         = 8
COMPASS     = 9
M4G         = 10
NDOF_FMC_OFF= 11
NDOF        = 12

class IMU:
    ''' @brief    
        @details  
    '''
     
    def __init__ (self, i2c):
         ''' @brief Initializes and returns an IMU object.
         '''
         self.i2c = i2c
#         I2C(1, I2C.MASTER)
#         self.i2c.init(I2C.MASTER, baudrate = 20000)
#         self.i2c.init(I2C.SLAVE, addr = 0x28)
    
    def set_opr_mode (self):
         ''' @brief 
         '''
         self.i2c.mem_write(0x0C, 0x28, 0x3D)

    def get_calib_stat (self):
         ''' @brief 
         '''
         cal_bytes = self.i2c.mem_read(1, 0x28, 0x35)
         calib_stat = (cal_bytes[0] & 0b11,
                      (cal_bytes[0] & 0b11 << 2) >> 2,
                      (cal_bytes[0] & 0b11 << 4) >> 4,
                      (cal_bytes[0] & 0b11 << 6) >> 6)
#         return calib_stat   # bits <7:6> 3: yes 0: no
#         print(calib_stat)
         print("Values:", calib_stat)
         print('\n')
         
#         deg = lsb/16
    def get_calib_coeff (self):
         ''' @brief 
         '''
         _cal_buf = bytearray(22)
         return self.i2c.mem_read(_cal_buf, 0x28, 0x55)
         
    def set_calib_coeff (self, coeff):
         ''' @brief 
         '''
         self.coeff = coeff
         self.i2c.mem_write(self.coeff, 0x28, 0x55)
         
    def get_angles (self):
         ''' @brief 
         '''
         _euler_buf = bytearray(6)
#         return self.i2c.mem_read(_euler_buf, 0x28, 0x1A)
#         print(_euler_buf[5])
         self.i2c.mem_read(_euler_buf, 0x28, 0x1A)
         eul_signed_ints = struct.unpack('hhh', _euler_buf)
         eul_vals = tuple(eul_int/16 for eul_int in eul_signed_ints)
         print('Euler Angles:', eul_vals)
#         print('unpacked:', struct.unpack('hhh', _euler_buf))
     
    def get_omegas (self):
         ''' @brief 
         '''
         _omega_buf = bytearray(6)
#         return self.i2c.mem_read(_omega_buf, 0x28, 0x14)
         self.i2c.mem_read(_omega_buf, 0x28, 0x14)
         omega_signed_ints = struct.unpack('hhh', _omega_buf)
         omega_vals = tuple(omega_int/16 for omega_int in omega_signed_ints)
         print('Angular Velocities:', omega_vals)
     
if __name__ == '__main__':
    import pyb
    i2c = pyb.I2C(1,I2C.MASTER)
    imu_drv = IMU(i2c)
    
    while True:
        try:
            imu_drv.set_opr_mode()
#            imu_drv.get_angles()
            imu_drv.get_omegas()
#            imu_drv.get_calib_stat()
            time.sleep(.2)
        except KeyboardInterrupt:
            print('Program Terminating')
            break
        
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        