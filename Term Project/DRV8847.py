''' @file                       DRV8847.py
    @brief                      A driver for configuring the motors.
    @details                    A driver used to configure the DRV8847 motors. 
    @author                     Ronan Shaffer
    @author                     Nishka Chawla
    @date                       10/25/2021
'''
import pyb
import utime


class DRV8847:
    '''  @brief    A motor driver class for the DRV8847 from TI.
         @details  Objects of this class can be used to configure the DRV8847
                    motor driver and to create one or moreobjects of the
                    Motor class which can be used to perform motor control.
    
     Refer to the DRV8847 datasheet here:
     https://www.ti.com/lit/ds/symlink/drv8847.pdf
     '''
     
    def __init__ (self, tim):
         ''' @brief         Initializes and returns a DRV8847 object.
             @details       Constructs motor objects in the Motor class.
             @param tim     Defines the number of the timer object.
         '''
#         self.IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
#         self.IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
#         
#         self.IN3 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
#         self.IN4 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
#         
#         self.nSLEEP = pyb.Pin(nSLEEP, pyb.Pin.OUT_PP)
#         self.nFAULT = pyb.Pin(nFAULT, pyb.Pin.IN)

         ## @brief   Defines the number of the timer object.
         self.tim = pyb.Timer(tim, freq = 20000)
#         self.faultInt = pyb.ExtInt(self.nFAULT, mode=pyb.ExtInt.IRQ_FALLING, 
#                                   pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
#         self.fault_cb_flag = 0
    
#    def enable (self):
#         ''' @brief Brings the DRV8847 out of sleep mode.
#         '''
#         self.faultInt.disable()    # Disable fault interrupt
#         self.nSLEEP.high()         # Re-enable the motor driver
#         utime.sleep_us(25)         # Wait for the fault pin to return high
#         self.faultInt.enable()     # Re-enable the fault interrupt
#    
#    def disable (self):
#         ''' @brief Puts the DRV8847 in sleep mode.
#         '''
#         self.nSLEEP.low()
#    
#    def fault_cb (self, IRQ_src):
#         ''' @brief Callback function to run on fault condition.
#             @param IRQ_src The source of the interrupt request.
#         '''
##         print('Fault detected')
#         self.fault_cb_flag = 1
#         self.disable()
    
    def motor (self, pinA, pinB, channel_A, channel_B):
         ''' @brief     Initializes and returns a motor object associated with the DRV8847.
             @return    An object of class Motor.
         '''
         
         return Motor(pinA, pinB, channel_A, channel_B, self.tim)
         
             
class Motor:
    ''' @brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given
                    DC motor.
    '''
    
    def __init__ (self, pinA, pinB, channel_A, channel_B, tim):
        ''' @brief             Initializes and returns a motor object associated with the DRV8847.
            @details           Creates a DRV8847 object that creates Motor objects using the method DRV8847.motor().
            @param pinA        Defines the pin for channel A of the motor.
            @param pinB        Defines the pin for channel B of the motor.
            @param channel_A   Spins the motor 'forwards'.
            @param channel_B   Spins the motor 'backwards'.
            @param tim         Defines the number of the timer object.
        '''
        ## @brief   Defines the number of the timer object.
        self.tim = tim
        
        ## @brief   Defines motor channel used to spin motor 'forwards'.
        self.channel_A = self.tim.channel(channel_A, pyb.Timer.PWM, pin=pinA)
        
        ## @brief   Defines motor channel used to spin motor 'backwards'.
        self.channel_B = self.tim.channel(channel_B, pyb.Timer.PWM, pin=pinB)
    
    def set_duty(self, duty):
        ''' @brief          Set the PWM duty cycle for the motor channel.
            @details        This method sets the duty cycle to be sent to the motor to the given level. Positive values
                            cause effort in one direction, negative values in the opposite direction.
            @param duty     A signed number holding the duty cycle of the PWM signal sent to the motor.
        '''
#        self.channel_1 = ch1
#        self.channel_2 = ch2

        ## @brief   A signed number holding the duty cycle of the PWM signal sent to the motor.
        self.duty = duty

        if self.duty > 0:
            
            self.channel_A.pulse_width_percent(self.duty)
            self.channel_B.pulse_width_percent(0)
            
        if self.duty < 0:
            
            self.channel_A.pulse_width_percent(0)
            self.channel_B.pulse_width_percent(-self.duty)
            
            
#if __name__ == "__main__":
#    
#    import pyb
#    print('hi')
#    
#    # Define pins
#    A15 = pyb.Pin(pyb.Pin.cpu.A15)
#    B0  = pyb.Pin(pyb.Pin.cpu.B0)
#    B1  = pyb.Pin(pyb.Pin.cpu.B1)
#    B2  = pyb.Pin(pyb.Pin.cpu.B2)
#    B4  = pyb.Pin(pyb.Pin.cpu.B4)
#    B5  = pyb.Pin(pyb.Pin.cpu.B5)
#
#    #Motor instantiation
#    motor_drv = DRV8847(3)
#    motor1 = motor_drv.motor(B4, B5, 1, 2)
#    motor2 = motor_drv.motor(B0, B1, 3, 4)
#    
#    while True:
#        
#        try:
#            print('hello')
#            motor1.set_duty(100)
#            motor2.set_duty(100)
#        except KeyboardInterrupt:
#            motor1.set_duty(0)
#            motor2.set_duty(0)
#            print('Program Terminating')
#            break
    