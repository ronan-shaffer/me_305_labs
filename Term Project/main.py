''' @file                       main.py
    @brief                      Runs task_user, task_motor, task_TouchPanel, and task_IMU methods.
    @details                    Iterates through methods in task classes and
                                instantiates share objects. 
                                This is the link to the FSM diagrams: 
                                https://imgur.com/a/XltHUfP
    @author                     Nishka Chawla
    @author                     Ronan Shaffer
    @date                       12/9/2021
'''

# All import statements should go near the top
import pyb
from pyb import I2C
import task_user
import task_motor
import task_TouchPanel
import task_IMU
import shares
import DRV8847
import micropython

def main():
    ''' @brief              Main loop running the program.
        @details            Main program iterates through task_user, task_motor,        
                            task_TouchPanel, and task_IMU methods to communicate 
                            between user and encoder.
    '''
    micropython.alloc_emergency_exception_buf(100)
    
    # Define pins
    ## @brief   Channel A Pin object for Motor 2
    B0  = pyb.Pin(pyb.Pin.cpu.B0)
    
    ## @brief   Channel B Pin object for Motor 2
    B1  = pyb.Pin(pyb.Pin.cpu.B1)
    
    ## @brief   Channel A Pin object for Motor 1
    B4  = pyb.Pin(pyb.Pin.cpu.B4)
    
    ## @brief   Channel B Pin object for Motor 2
    B5  = pyb.Pin(pyb.Pin.cpu.B5)

    #Motor instantiation
    ## @brief   Motor driver object used to configure the DRV8847 motors.
    motor_drv = DRV8847.DRV8847(3)
    
    ## @brief   Instantiation of Motor 1 object
    motor1 = motor_drv.motor(B4, B5, 1, 2)
    
    ## @brief   Instantiation of Motor 1 object
    motor2 = motor_drv.motor(B0, B1, 3, 4)
    
    ## @brief   Object containing an index of motor objects.
    motors = (motor1, motor2)
    
    ## @brief   A shares.Share object for Motor 1 gains. 
    #  @details Contains list in order of X Linear Position gain, Y Angular 
    #           Velocity gain, X Linear Velocity gain, Y Angular Velocity gain.
    gains1 = (shares.Share(0),shares.Share(-22),shares.Share(0),shares.Share(-0.11))
    
    ## @brief   A shares.Share object for Motor 2 gains. 
    #  @details Contains list in order of Y Linear Position gain, X Angular 
    #           Position gain, Y Linear Velocity gain, X Angular Velocity gain.
    gains2 = (shares.Share(0),shares.Share(22),shares.Share(0),shares.Share(0.11))
    
    ## @brief   A shares.Share object for the touch panel state variables.
    #  @details Contains list of touch panel state variables in order of x, y, x_dot, y_dot.
    state_panel = (shares.Share(0),shares.Share(0),shares.Share(0),shares.Share(0))

    ## @brief   A shares.Share object for the IMU state variables.
    #  @details Contains list of IMU state variables in order of theta_x, theta_y, thetadot_x, thetadot_y.
    state_IMU = (shares.Share(0),shares.Share(0),shares.Share(0),shares.Share(0))
    
    ## @brief   A shares.Share object for the task period
    #  @details The period, in microseconds, between runs of the task.
    period = shares.Share(20000)
    
    ## @brief   A shares.Share object for the enable flag.
    #  @details A boolean flag used to enable and disable the motors.
    enable_flag = shares.Share(0)
    
    ## @brief   A shares.Share object for the balance flag.
    #  @details A boolean flag used to enable the closed loop torque control of the platform. 
    balance_flag = shares.Share(0)
    
    ## @brief   A shares.Share object for the IMU calibration flag.
    #  @details A boolean flag used to enable IMU calibration.
    imu_c_flag = shares.Share(0)
    
    ## @brief   A shares.Share object for the touch panel contact flag.
    #  @details A boolean flag used to check for contact on the Touch Panel.
    cf = shares.Share(0)
    
    ## @brief   A shares.Share object for a touch panel calibration index. 
    #  @details An index variable used to calibrate the touch panel. 
    cdx = shares.Share(0)
    
    ## @brief   A task_user object.
    #  @details Instantiates object in the task_user class to run method in main.
    task1 = task_user.Task_User(period, gains1, gains2, motors, cf, cdx, balance_flag, enable_flag, imu_c_flag, state_IMU, state_panel)
    
    ## @brief   A task_TouchPanel object.
    #  @details Instantiates object in task_TouchPanel class to run method in main.
    task2 = task_TouchPanel.Task_TouchPanel(cf, cdx, state_panel)
    
    ## @brief   A task_IMU object.
    #  @details Instantiates object in task_IMU class to run method in main.
    task3 = task_IMU.Task_IMU(state_IMU, imu_c_flag)
    
    ## @brief   A task_motor object.
    # @details Instantiates object in task_motor class to run method in main
    task4 = task_motor.Task_Motor(period, gains1, gains2, state_panel, state_IMU, motors, motor_drv, cf, balance_flag, enable_flag)
    
    _task_list = [task1, task2, task3, task4]
    
    while (True):
        # Attempt to run FSM unless Ctrl+C is hit
        try:
            for task in _task_list:
                task.run()  
            
        #Look for Ctrl+C which triggers a KeyboardInterrupt
        except KeyboardInterrupt:
            break # Breaks out of the FSM while loop
    
    # Action taken once while loop is terminated
    motor1.set_duty(0)
    motor2.set_duty(0)
    print('Program Terminating')


if __name__ == '__main__':
    main()
