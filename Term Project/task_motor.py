''' @file                       task_motor.py
    @brief                      Interfaces with DRV8847 driver.
    @details                    Defines Task_Motor class and run method.
    @author                     Nishka Chawla
    @author                     Ronan Shaffer
    @date                       12/9/2021
'''

import pyb
import time
import DRV8847
import closedloop
import micropython
from ulab import numpy
import array as array

_S0_INIT = 0

_S1_RUN  = 1

_S2_STOP = 2

# K_11 = micropython.const(-0.3*0)

# K_12 = micropython.const(-22)

# K_13 = micropython.const(-0.05*0)

# K_14 = micropython.const(-0.11)

# K_21 = micropython.const(-0.33*0)

# K_22 = micropython.const(20)

# K_23 = micropython.const(-0.05*0)

# K_24 = micropython.const(0.11)

class Task_Motor:
    ''' @brief                  Interface with DRV8847 motors.
        @details                Communicates between task_user, task_IMU, task_TouchPanel 
                                and motor driver to perform motor torque control to balance 
                                platform.
    '''
    
    def __init__(self, period, gains1, gains2, state_panel, state_IMU, motors, motor_drv, cf, balance_flag, enable_flag):
        ''' @brief                  Interfaces with DRV8847 motors.
            @details                Communicates between task_user, task_IMU, task_TouchPanel 
                                    and motor driver to perform motor torque control to balance 
                                    platform.
            @param period           The period, in microseconds, between runs 
                                    of the task.
            @param gains1           The proportional gain values for controller 1.
            @param gains2           The proportional gain values for controller 2.
            @param state_panel      State variables from touch panel readings.
            @param state_IMU        State variables from IMU readings.
            @param motors           Object containing an index of motor objects.
            @param motor_drv        A motor driver object in the DRV8847 class. 
            @param cf               A boolean flag used to check for contact on the Touch Panel.
            @param balance_flag     A boolean flag used to enable the closed loop torque control of the platform. 
            @param enable_flag      A boolean flag used to enable and disable the motors.
        '''       
        ## @brief   A shares.Share object for Motor 1 gains. 
        #  @details Contains list in order of X Linear Position gain, Y Angular 
        #           Velocity gain, X Linear Velocity gain, Y Angular Velocity gain.
        self.gains1 = gains1
        
        ## @brief   A shares.Share object for Motor 2 gains. 
        #  @details Contains list in order of Y Linear Position gain, X Angular 
        #           Position gain, Y Linear Velocity gain, X Angular Velocity gain.
        self.gains2 = gains2
        
        ## @brief   A shares.Share object for the touch panel state variables.
        #  @details Contains list of touch panel state variables in order of x, y, x_dot, y_dot.
        self.state_panel = state_panel
        
        ## @brief   A shares.Share object for the IMU state variables.
        #  @details Contains list of IMU state variables in order of theta_x, theta_y, thetadot_x, thetadot_y.
        self.state_IMU = state_IMU
        
        self.duty_const = micropython.const(100*2.21/(4*13.8*12))
        
        ## @brief   A shares.Share object for the balance flag.
        #  @details A boolean flag used to enable the closed loop torque control of the platform. 
        self.balance_flag = balance_flag
        
        ## @brief   A shares.Share object for the enable flag.
        #  @details A boolean flag used to enable and disable the motors.
        self.enable_flag = enable_flag
        
        ## @brief   A shares.Share object for motor objects.
        #  @details  Object containing an index of motor objects.
        self.motors = motors
        
        ## @brief   A motor driver object in the DRV8847 class.
        self.motor_drv = motor_drv
        
        ## @brief   A shares.Share object for the touch panel contact flag.
        #  @details A boolean flag used to check for contact on the Touch Panel.
        self.cf = cf
        
        ## @brief   The state to run on the next iteration of the finite state machine.
        self._state = _S1_RUN
        
        ## @brief   Desired torque in the x-direction.
        self.Tx = 0
        
        ## @brief   Desired torque in the y-direction.
        self.Ty = 0

        
    def run(self):
        ''' @brief      Iterates through cooperative tasks.
            @details    Communicates between task_user, task_IMU, task_TouchPanel 
                        and motor driver to perform motor torque control to balance 
                        platform.
        '''
        ## @brief   Array of chosen proportional gains for motor 1. 
        K_1 = numpy.array([self.gains1[0].read(), self.gains1[1].read(), self.gains1[2].read(), self.gains1[3].read()])
        
        ## @brief   Array of chosen proportional gains for motor 2. 
        K_2 = numpy.array([self.gains2[0].read(), self.gains2[1].read(), self.gains2[2].read(), self.gains2[3].read()])

#        if self._state == _S1_RUN:
            
#            if self.enable_flag.read() == 0:
#                self.transition_to(_S2_STOP)
            
        if self.balance_flag.read() == 1:
            ## @brief   Array of x, theta_y, x_dot, theta_ydot.
            state_x = numpy.array([[self.state_panel[0].read()], [self.state_IMU[1].read()], [self.state_panel[2].read()], [self.state_IMU[3].read()]])
            
            ## @brief   Array of y, theta_x, y_dot, theta_xdot.
            state_y = numpy.array([[self.state_panel[1].read()], [self.state_IMU[0].read()], [self.state_panel[3].read()], [self.state_IMU[2].read()]])
            
            self.Tx = numpy.dot(-K_1, state_x)            
            self.Ty = numpy.dot(-K_2, state_y)
            
            self.motors[0].set_duty(int(self.duty_const*self.Tx[0]))
            self.motors[1].set_duty(int(self.duty_const*self.Ty[0]))
            
#        elif self._state == _S2_STOP:
#            
#            self.motors[0].set_duty(0)
#            self.motors[1].set_duty(0)
#            print('disabled test')
            
#            if self.enable_flag.read() == 1:
#                print('enable test')
#                self.transition_to(_S1_RUN)
            
    def transition_to(self, _new_state):
        ''' @brief      Transitions the FSM to a new state
            @param      new_state The state to transition to next.
        '''
        self._state = _new_state