''' @file                       touchpanel.py
    @brief                      A driver for reading from resistive touch panel.
    @details                    A driver used to read position from the resistive touch panel. 
    @author                     Ronan Shaffer
    @author                     Nishka Chawla
    @date                       12/9/2021
'''

from pyb import Pin
from pyb import ADC
import array as array
import micropython
import utime
# import os
# filename = "RT_cal_coeffs.txt"

_o = micropython.const(Pin.OUT_PP)
_i = micropython.const(Pin.IN)
_A0 = micropython.const(Pin.cpu.A0)
_A1 = micropython.const(Pin.cpu.A1)
_A6 = micropython.const(Pin.cpu.A6)
_A7 = micropython.const(Pin.cpu.A7)

_xw = micropython.const(170/3700)
_xo = micropython.const(85)
_yw = micropython.const(82/3700)
_yo = micropython.const(45)


class Touch_Panel:
    ''' @brief                  Interface with resistive touch panel.
        @details                Class containing xyz_scan, and get_readings methods to 
                                read positional data from the resistive touch panel.
    '''
    
    def __init__(self):
        ''' @brief              Interface with resistive touch panel.
            @details            Class containing methods to read positional data
                                from the resistive touch panel.
        '''
#        ## @brief   Instantiation of x position var
#        self.x = 0
        
#        ## @brief   Reads and stors calibration status from IMU register.
#        self.y = 0
        
        ## @brief   Instantiation of x- position reading.
        self.xr = 0
        
        ## @brief   Instantiation of y- position reading.
        self.yr = 0
        
#    @micropython.native
#    def x_scan(self):
#        # range: 220-3800
#        _xm = Pin(_A1, _o)
#        _yp = Pin(_A6, _i)
#        _xp = Pin(_A7, _o)
#        
#        _xp.high()
#        _xm.low()
#        
#        _a = ADC(Pin(_A0))
#        _v = _a.read()
#        self.x = int((_v*_xw)-_xo)
#        return(self.x)
#       
#    @micropython.native
#    def y_scan(self):
#        # range: 380-3600
#        _ym = Pin(_A0, _o)
#        _yp = Pin(_A6, _o)
#        _xp = Pin(_A7, _i)
#        
#        _yp.high()
#        _ym.low()
#        
#        _a = ADC(Pin(_A1))
#        _v = _a.read()
#        self.y = int((_v*_yw)-_yo)
#        return(self.y)
#        
#    @micropython.native
#    def z_scan(self):
#        _ym = Pin(_A0, _o)
#        _xm = Pin(_A1, _o)
#        _yp = Pin(_A6, _o)
#        
#        _yp.high()
#        _xm.low()
#
#        _a = ADC(Pin(_A7))
#        _v = _a.read()
#        
#        if int(_v) > micropython.const(40):
#            cf = micropython.const(1)
#        else:
#            cf = micropython.const(0)
#        
#        return(cf)
        
    @micropython.native
    def xyz_scan(self):
        '''  @brief             Returns contact flag detecting touch on the touch panel. 
             @details           Method to detect contact on the resistive touch panel..
             @return            Boolean flag detecting contact on the touch panel.
        '''
         
        _ym = Pin(_A0, _o)
        _xm = Pin(_A1, _o)
        _yp = Pin(_A6, _o)
        
        _yp.high()
        _xm.low()

        _a = ADC(Pin(_A7))
        _v = _a.read()
        
        if int(_v) > micropython.const(40):
            ## @brief   A boolean flag used to check for contact on the touch panel.
            self.cf = micropython.const(1)
            _xm = Pin(_A1, _o)
            _yp = Pin(_A6, _i)
            _xp = Pin(_A7, _o)
            
            _xp.high()
            _xm.low()
            
            _ax = ADC(Pin(_A0))
            self.xr = _ax.read()
#            self.x = float((self.xr*_xw)-_xo)
    
            _ym = Pin(_A0, _o)
            _yp = Pin(_A6, _o)
            _xp = Pin(_A7, _i)
            
            _yp.high()
            _ym.low()
            
            _ay = ADC(Pin(_A1))
            self.yr = _ay.read()
#            self.y = float((self.yr*_yw)-_yo)
        else:
            self.cf = micropython.const(0)
        
        return(self.cf)

    def get_readings(self):
        '''  @brief             Returns positional data from touch panel.
             @details           Method to read x- position, y- position, and contact flag from the touch panel.
             @return            List of x- position, y- position, and contact flag readings.
        '''
         
        ## @brief   Tuple of x- position, y- position, and contact flag readings.
        self.readings = (self.xr, self.yr, self.cf)
        
        ## @brief   List of x- position, y- position, and contact flag readings.
        self.reading = list(self.readings)
        
        return(self.reading)
        
#    def get_position(self):
#        self.positions = (self.x, self.y, self.cf)
#        self.position = list(self.positions)
#        return(self.position)
        
#    def calibrate(self):
#        pass
#        
        
#if __name__ == '__main__':
#    
#    panel_drv = Touch_Panel()    
#    _start_time = utime.ticks_us()
#    
#    for n in range(0, 99):
#
#        panel_drv.xyz_scan()
#    
#    _next_time = utime.ticks_us()
#    avg_run_time = utime.ticks_diff(_next_time, _start_time)
#    print(avg_run_time/100)
        
        
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        