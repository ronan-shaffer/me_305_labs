''' @file                       task_IMU.py
    @brief                      Interfaces with IMU.
    @details                    Defines Task_IMU class and run method.
    @author                     Nishka Chawla
    @author                     Ronan Shaffer
    @date                       12/9/2021
'''
from pyb import I2C
import IMU
import os

## @brief   Instantiation of variable referencing calibration coefficients file. 
filename = "IMU_cal_coeffs.txt"

class Task_IMU:
    ''' @brief                  Interface with IMU.
        @details                Communicates between task_user, task_motor and IMU driver
                                to read platform orientation.
    '''
    
    def __init__(self, state_IMU, imu_c_flag):
        ''' @brief                  Interfaces with IMU driver.
            @details                Communicates between task_user, task_motor, and IMU
                                    driver to relay IMU orientation information.
            @param state_IMU        Contains list of IMU state variables in order 
                                    of theta_x, theta_y, thetadot_x, thetadot_y.
            @param imu_c_flag       A boolean flag used to enable IMU calibration.
        '''
        ## @brief       Instantiation and initialisation of i2c object as a controller.
        i2c = I2C(1,I2C.MASTER)
        
        ## @brief       IMU driver object calling the driver class IMU.
        self.imu_drv = IMU.IMU(i2c)
        
        ## @brief       Method to set the IMU operating mode to NDOF.
        self.imu_drv.set_opr_mode()
        
        ## @brief       An index used to iterate through IMU calibration once.
        self._runs = 0
        
        ## @brief       An index used to write each calibration coefficient to the text file.
        self.x = 0
        
        # ## @brief     A shares.Share object for the enable flag.
        # self.y = 0
        
        ## @brief       A boolean flag used to perform auto calibration.
        self.bool = 0
        
        ## @brief       A shares.Share object for the IMU state variables.
        self.state_IMU = state_IMU
        
        ## @brief       A shares.Share object for the IMU calibration flag.
        self.imu_c_flag = imu_c_flag

        
    def autocalibrate(self):
        ''' @brief      Performs automatic calibration of the IMU.                  
            @details    Reads calibration coefficients from IMU_cal_coeffs.txt
                        and writes them to the IMU register for calibration.
        '''
        
        with open(filename, 'r') as f:
            # Read the first line of the file
            ## @brief   Variable which reads and stores content of IMU_cal_coeffs.txt file.
            cal_data_string = f.readline()
            # Split the line into multiple strings
            # and then convert each one to an int
            ## @brief   Splits IMU_cal_coeffs.txt string into 22 integers, and converts them to bytes.
            cal_buf = bytes(int(cal_value) for cal_value in cal_data_string.strip().split(','))
            self.imu_drv.set_calib_coeff(cal_buf)
            print('IMU calibration coefficients = ', cal_buf)
            print('IMU Calibration complete.')
            self._runs += 1
        
    def manualcalibrate(self):
        ''' @brief      Performs manual calibration of the IMU.             
            @details    Requires the user to manually calibrate the IMU and 
                        creates an IMU_cal_coeffs.txt file with calibrated coefficients.                 
        '''
        
        self.bool = 1
        with open(filename, 'w') as f:
            # Perform manual calibration
            calib_stat = self.imu_drv.get_calib_stat()
            print(calib_stat)
            if calib_stat == (3,3,3,3):
                coeffs = list(self.imu_drv.get_calib_coeff())
                if self.x < len(coeffs):
                    coeffs[self.x] = int(coeffs[self.x])
                    coeffs[self.x] = hex(coeffs[self.x])
                    self.x += 1
                else:
                    (R55, R56, R57, R58, R59, R5A, R5B, R5C, R5D, R5E, R5F, R60, R61, R62, R63, R64, R65, R66, R67, R68, R69, R6A) = coeffs
                    var = f'{R55:#x},{R56:#x},{R57:#x},{R58:#x},{R59:#x},{R5A:#x},{R5B:#x},{R5C:#x},{R5D:#x},{R5E:#x},{R5F:#x},{R60:#x},{R61:#x},{R62:#x},{R63:#x},{R64:#x},{R65:#x},{R66:#x},{R67:#x},{R68:#x},{R69:#x},{R6A:#x}\r\n'
                    f.write(var)
                    print('IMU calibration coefficients = ', var)
                    print('IMU calibration complete.')
                    self._runs += 1
            
    def run(self):
        ''' @brief      Iterates through cooperative tasks.                   
            @details    Performs IMU calibration and communicates between task_motor 
                        and IMU driver to read and write IMU state variables.          
        '''
        
        if self._runs < 1:
            
            if self.imu_c_flag.read() == 1:
            
                if filename in os.listdir() and self.bool == 0:
                    self.autocalibrate()
                else:
                    self.manualcalibrate()
        else:
            ## @brief   Angles object which stores euler angles from IMU driver.
            self.angles = self.imu_drv.get_angles()
            ## @brief   Omegas object which stores angular velocities from IMU driver.
            self.omegas = self.imu_drv.get_omegas()
            self.state_IMU[0].write(self.angles[1])
            self.state_IMU[1].write(self.angles[2])
            self.state_IMU[2].write(self.omegas[0])
            self.state_IMU[3].write(self.omegas[1])
#            print(self.state_IMU[0].read(), self.state_IMU[1].read(), self.state_IMU[2].read(), self.state_IMU[3].read() )
#            time.sleep(.02)
            
    
    