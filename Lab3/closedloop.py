''' @file                       closedloop.py
    @brief                      A driver for closed loop control
    @details
    @author                     Ronan Shaffer
    @author                     Nishka Chawla
    @date                       10/29/2021
'''

import pyb

class ClosedLoop:
    ''' @brief                  Closed-loop controller class
        @details
    '''
    
    def __init__(self, Kp, ref, msr, sat_max, sat_min):
        ''' @brief
            @details
        '''
        ## Initialisation of Kp variable
        self.Kp = Kp
        
        ## Initialisation of error variable
        self.error = 0
        
        ## Initialisation of reference variable
        self.ref = ref
        
        ## Initialisation of measured variable
        self.msr = msr
        
        ## Saturation High Limit
        self.sat_max = sat_max
        
        ## Saturation Low Limit
        self.sat_min = sat_min
        
        ## Initialisation of L variable
        self.L = 0

    def run(self):
        ''' @brief
            @details
            @return             Computes and returns the actuation value based 
                                on the measured and reference values.
        '''        
#        add integral stuff if we want
        
        self.error = self.ref - self.msr
        self.L = self.Kp*self.error
        
        if self.L > self.sat_max:
            self.L = self.sat_max
            return self.L
        if self.L < self.sat_min:
            self.L = self.sat_min
            return self.L

    def get_Kp(self):
        ''' @brief              Allow modification of the gain value.
            @details
        '''  
        
        return self.Kp

    def set_Kp(self, Kp):
        ''' @brief              Allow modification of the gain value.   
            @details
            @param  
        '''
        
        self.Kp = Kp
        print('Setting Kp value')
  
