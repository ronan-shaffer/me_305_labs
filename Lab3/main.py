''' @file                       main.py
    @brief                      Runs task_encoder and task_user methods.
    @details                    Iterates through methods in task classes and
                                instantiates share objects. 
                                This is the link to the FSM diagrams: 
                                https://imgur.com/a/XltHUfP
    @author                     Nishka Chawla
    @author                     Ronan Shaffer
    @date                       10/19/2021
'''


# All import statements should go near the top
import pyb
import task_encoder # imports module "task_example.py" (not the class)
import task_user 
import task_motor
import shares
import encoder
import DRV8847
import micropython


def main():
    ''' @brief              Main loop running the program.
        @details            Main program iterates through task_user and task_encoder
                            methods to communicate between user and encoder.
    '''
    
    # Define pins
    _A15 = pyb.Pin(pyb.Pin.cpu.A15)
    _B0  = pyb.Pin(pyb.Pin.cpu.B0)
    _B1  = pyb.Pin(pyb.Pin.cpu.B1)
    _B2  = pyb.Pin(pyb.Pin.cpu.B2)
    _B4  = pyb.Pin(pyb.Pin.cpu.B4)
    _B5  = pyb.Pin(pyb.Pin.cpu.B5)
    _B6  = pyb.Pin(pyb.Pin.cpu.B6)
    _B7  = pyb.Pin(pyb.Pin.cpu.B7)
    _C6  = pyb.Pin(pyb.Pin.cpu.C6)
    _C7  = pyb.Pin(pyb.Pin.cpu.C7)

    
    ## @brief   Encoder 1 object in the task_encoder class.
    encoder1 = encoder.Encoder(_B6, _B7, 4)
    
    ## @brief   Encoder 2 object in the task_encoder class.
    encoder2 = encoder.Encoder(_C6, _C7, 8)
    
    ## @brief   A shares.Share object for encoder objects.
    #  @details  Object containing an index of encoder objects. 
    encoders = (encoder1, encoder2)
    
    #Motor instantiation
    ## @brief   A motor driver object in the DRV8847 class.
    motor_drv = DRV8847.DRV8847(_A15, _B2, 3)
    
    ## @brief   Motor 1 object in the task_motor class.
    motor1 = motor_drv.motor(_B4, _B5, 1, 2)
    
    ## @brief   Motor 2 object in the task_motor class.
    motor2 = motor_drv.motor(_B0, _B1, 3, 4)
    
    ## @brief   A shares.Share object for motor objects.
    #  @details  Object containing an index of motor objects.
    motors = (motor1, motor2)
    
    ## @brief   A shares.Share object for positions.
    #  @details The most recent orientation read by the encoders.
    positions = (shares.Share(0),shares.Share(0))
    
    ## @brief   A shares.Share object for deltas.
    #  @details The number of ticks between the two most recent positions
    #           recorded by the encoders.
    deltas = (shares.Share(0),shares.Share(0))
    
    ## @brief   A shares.Share object for the zero flags.
    #  @details Boolean flags used to instruct the encoder task to set the
    #           most recent encoder position to zero.
    z_flags = (shares.Share(0),shares.Share(0))
    
    ## @brief   A shares.Share object for the motor duty cycle.
    #  @details Sets the duty cycle of the motors in the task_motor class.
    duties = (shares.Share(0),shares.Share(0))
    
    ## @brief   A shares.Share object for the task period.
    #  @details The period, in microseconds, between runs of the task.
    period = shares.Share(50000)
    
    ## @brief   A shares.Share object for enabling the motors.
    #  @details A boolean flag used to enable and disable the motors.
    enable_flag = shares.Share(0)
    
    ## @brief   A shares.Share object for fault detection.
    #  @details A boolean flag to inform the user of fault detection in the 
    #           motors.
    fault_user_flag = shares.Share(0)
    
    ## @brief   A task_user object.
    #  @details Instantiates object in task_user class to run method in main.
    task1 = task_user.Task_User(period, z_flags, enable_flag, fault_user_flag, positions, deltas, duties, encoders, motors)
    
    ## @brief   A task_encoder object.
    #  @details Instantiates object in task_encoder class to run method in main.
    task2 = task_encoder.Task_Encoder(period, z_flags, positions, deltas, encoders)
    
    ## @brief   A task_motor object.
    #  @details Instantiates object in task_motor class to run method in main.
    task4 = task_motor.Task_Motor(period, z_flags, enable_flag, fault_user_flag, positions, deltas, duties, motors, motor_drv)


    _task_list = [task1, task2, task4]
    
    while (True):
        # Attempt to run FSM unless Ctrl+C is hit
        try:
            for task in _task_list:
                task.run()  
            
        #Look for Ctrl+C which triggers a KeyboardInterrupt
        except KeyboardInterrupt:
            motor_drv.disable()
            break # Breaks out of the FSM while loop
    
    # Action taken once while loop is terminated
    print('Program Terminating')


if __name__ == '__main__':
    main()
