''' @file                       encoder.py
    @brief                      A driver for reading from Quadrature Encoders
    @details                    Driver reads encoder data and writes to encoder 
                                task, and implements signals from encoder task
                                in hardware
    @author                     Ronan Shaffer
    @author                     Nishka Chawla
    @date                       10/25/2021
'''

import pyb

class Encoder:
    ''' @brief                  Class containing methods in encoder driver.
        @details                Class containing update, get_position, 
                                set_position, and get_delta methods to send 
                                information to task_encoder.
    '''
    
    def __init__(self, pinA, pinB, tim_num):
        ''' @brief              Constructor for Encoder Class.  
            @details            Constructs a timer object in Encoder class.
            @param pinA         Defines the pin for channel 1 of the timer.
            @param pinB         Defines the pin for channel 2 of the timer.
            @param tim_num      Defines the number of the timer object.
        '''
        
        self._ref_count = 0
        self._current_pos = 0
        
        ## @brief   Overflow for Encoder timer counter
        #  @details Defines 16-bit constant value to check delta against 
        #           overflow and correct as needed.
        self.period = 65535
        
        ## @brief   Timer object used for encoder counting.
        self.tim = pyb.Timer(tim_num, prescaler = 0, period = self.period)
        
        ## @brief   Encoder Channel for a timer object.
        self.tch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin=pinA)
        
        ## @brief   Encoder Channel for a timer object.
        self.tch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin=pinB)
        
        
    def update(self):
        ''' @brief              Updates encoder position and delta.
            @details            Tracks the current encoder position and 
                                corrects for overflow.
            @return             The position of the motor shaft.
        '''        
        ## @brief   The current tick count.
        self.update_count = self.tim.counter()
        
        ## @brief   Ticks between two most recent encoder positions.
        #  @details Calculates number of ticks between current position and
        #           most recent previous position and corrects for overflow.
        self.delta = self.update_count - self._ref_count
        if self.delta > 0 and self.delta > self.period/2:
            self.delta -= self.period
        if self.delta < 0 and abs(self.delta) > self.period/2:
            self.delta += self.period
            
        self._ref_count = self.tim.counter()
        
        self._current_pos += self.delta
        
        return self._current_pos
        
    def get_position(self):
        ''' @brief              Returns encoder position.
            @details            A function that returns encoder position.
            @return             The position of the motor shaft.
        '''  
        return self._current_pos
    
    def set_position(self, position):
        ''' @brief              Sets encoder position to specified value.
            @details            A function that sets the encoder position 
                                to a specified value.
            @param  position    The new position of the motor shaft
        '''
        self._current_pos = position

    def get_delta(self):
        ''' @brief              Returns encoder delta.
            @details            A function that returns the delta value.
            @return             The change in position of the motor shaft
                                between the two most recent updates
        '''
        return self.delta