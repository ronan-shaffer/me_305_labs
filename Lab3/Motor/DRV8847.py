''' @file DRV8847.py
 '''

import pyb
import time


class DRV8847:
    ''' @brief    A motor driver class for the DRV8847 from TI.
         @details  Objects of this class can be used to configure the DRV8847
                    motor driver and to create one or moreobjects of the
                    Motor class which can be used to perform motor control.
    
     Refer to the DRV8847 datasheet here:
     https://www.ti.com/lit/ds/symlink/drv8847.pdf
     '''
     
    def __init__ (self, timer_num, nSLEEP, nFAULT):
         ''' @brief Initializes and returns a DRV8847 object.
         '''
         self.IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
         self.IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
         
         self.IN3 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
         self.IN4 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
         
         self.nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
         self.nFAULT = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN)
         
         self.tim3 = pyb.Timer(3, freq = 20000)
         
         self.faultInt = pyb.ExtInt(self.nFAULT, mode=pyb.ExtInt.IRQ_FALLING, 
                                   pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)

         pass
    
    def enable (self):
         ''' @brief Brings the DRV8847 out of sleep mode.
         '''
         self.faultInt.disable()
         self.nSLEEP.high()
         time.sleep(.025)
         self.faultInt.enable()
    
    def disable (self):
         ''' @brief Puts the DRV8847 in sleep mode.
         '''
         self.nSLEEP.low()
    
    def fault_cb (self, IRQ_src):
         ''' @brief Callback function to run on fault condition.
             @param IRQ_src The source of the interrupt request.
         '''
         print('Fault detected')
         self.disable()
         pass
    
    def motor (self, Motornum):
         ''' @brief Initializes and returns a motor object associated with the DRV8847.
             @return An object of class Motor
         '''
         if Motornum ==1:
             t3ch1 = self.tim3.channel(1, pyb.Timer.PWM, pin=self.pinB4)
             t3ch2 = self.tim3.channel(2, pyb.Timer.PWM, pin=self.pinB5)
             return Motor(t3ch1, t3ch2)
         else:
             t3ch3 = self.tim3.channel(3, pyb.Timer.PWM, pin=self.pinB0)
             t3ch4 = self.tim3.channel(4, pyb.Timer.PWM, pin=self.pinB1)
             return Motor(t3ch3, t3ch4)
             
     
class Motor:
    ''' @brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given
                    DC motor.
    '''
    
    def __init__ (self, ch1, ch2):
        ''' @brief Initializes and returns a motor object associated with the DRV8847.
            @details Objects of this class should not be instantiated directly. 
            Instead create a DRV8847 object and use that to create Motor objects using the method DRV8847.motor().
        '''
        pass
    
    def set_duty(self,duty):
        ''' @brief Set the PWM duty cycle for the motor channel.
            @details This method sets the duty cycle to be sent to the motor to the given level. Positive values
            cause effort in one direction, negative values in the opposite direction.
            @param duty A signed number holding the duty cycle of the PWM signal sent to the motor
        '''
        self.channel_1 = ch1
        self.channel_2 = ch2
    
    
if __name__ == '__main__':
    
    motor_drv = DRV8847()
    motor_1 = motor_drv.motor()
    motor_2 = motor_drv.motor()
    
    # Enable the motor driver
    motor_drv.enable()
    
    
    # Set the duty cycle of the first motor to 40 percent and the duty cycle of
    # the second motor to 60 percent
    
    motor_1.set_duty(40)
    motor_2.set_duty(60)
    

