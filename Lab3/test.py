# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 16:12:51 2021

@author: melab15
"""

import pyb

IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
 
IN3 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
IN4 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
 
nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
nFAULT = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN)
 
tim3 = pyb.Timer(3, freq = 20000)
 
t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin= pyb.Pin.cpu.B4)
t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin= pyb.Pin.cpu.B5)
t3ch3 = tim3.channel(3, pyb.Timer.PWM, pin= pyb.Pin.cpu.B0)
t3ch4 = tim3.channel(4, pyb.Timer.PWM, pin= pyb.Pin.cpu.B1)

 
        
if __name__ == '__main__':

#    motor_drv = DRV8847()
#    motor_1 = motor_drv.motor()
#    motor_2 = motor_drv.motor()
#    
    # Enable the motor driver
#    motor_drv.enable()
    
    
    # Set the duty cycle of the first motor to 40 percent and the duty cycle of
    # the second motor to 60 percent
    
#    motor_1.set_duty(40)
#    motor_2.set_duty(60)
#    
#    return Motor(t3ch1, t3ch2)
#    
#    return Motor(t3ch3, t3ch4)
    
    while (True):
        try:
            nSLEEP.high()
            t3ch1.pulse_width_percent(0)
            t3ch2.pulse_width_percent(0)
            
            t3ch3.pulse_width_percent(0)
            t3ch4.pulse_width_percent(0)
            
        except KeyboardInterrupt:
            nSLEEP.low()
            break