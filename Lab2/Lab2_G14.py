# -*- coding: utf-8 -*-
"""
Created on Tue Oct  5 16:17:17 2021

@author: Ronan Shaffer, Nishka Chawla
"""

import pyb
import time
import utime

pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
tim4 = pyb.Timer(4, period = 65535, prescaler = 0)
t4ch1 = tim4.channel(1, pyb.Timer.ENC_AB, pin=pinB6)
t4ch2 = tim4.channel(2, pyb.Timer.ENC_AB, pin=pinB7)


pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)
tim3 = pyb.Timer(3, period = 65535, prescaler = 0)
t3ch1 = tim3.channel(1, pyb.Timer.ENC_AB, pin=pinC6)
t3ch2 = tim3.channel(2, pyb.Timer.ENC_AB, pin=pinC7)

while True:
    try:
        time.sleep(0.5)
        print(tim4.counter())
        print(tim3.counter())
        
    except KeyboardInterrupt:   # If keyboard interrupt, exit loop
        break
    
print('Program Terminated')

