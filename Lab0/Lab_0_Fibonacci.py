# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 15:18:25 2021

@author: Ronan Shaffer
"""

def fib (idx):
    # define function
    f_0 = 0
    f_1 = 1
    i = 0
    if idx == 0:
        return f_0
    elif idx == 1:
        return f_1
    else:
        for i in range (1, idx):
            # memoization method to quickly calculate Fibonacci number
            f_2 = f_1 + f_0
            f_0 = f_1
            f_1 = f_2
    return f_2
    
if __name__ == '__main__':
    while True:
        val = input('Please enter a positive integer: ')
        # convert string to integer
        try:
            # check if input is valid
            idx = int(val)
            if idx < 0:
                print('Sorry, integer must be positive. Please try again.')
                continue
        except ValueError:
            print("That's not an integer! Please try again.")
        else:
            # run function for valid index
            print("Thank you. Calculating Fibonacci number of given index...")
            fib(idx)
            print("The Fibonacci number of the index", idx, "is", fib(idx))

    
