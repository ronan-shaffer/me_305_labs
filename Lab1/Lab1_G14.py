# -*- coding: utf-8 -*-
"""
@file Lab1_G14.py
@brief Cycles through 3 different LED patterns
@details This program uses a Finite State Machine to cycle through 3 different
states representing 3 different waveforms: square wave, sine wave, and sawtooth
wave. Upon startup, user is informed to press the blue button connected to PC13
to cycle through each LED pattern to display the above waveforms in their
respective order.
@author Ronan Shaffer
@author Nishka Chawla
@date Sep 28 15:23:21 2021
"""
import time
import pyb
import math
import utime

_pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
_pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
_tim2 = pyb.Timer(2, freq = 20000)
_t2ch1 = _tim2.channel(1, pyb.Timer.PWM, pin=_pinA5)

##  @brief Causes state transition to next LED pattern.
#   @details Global variable that gets reassigned a value of true when button 
#   interrupt is triggered, incrementing the state variable so that the main 
#   loop cycles to the next LED pattern.
button_push = 0


def onButtonPressFCN(IRQ_src):
    '''@brief Defines interrupt service routine triggered by button press
       @details This function defines the interrupt handler action that follows
       the downstroke of the user initiated button press. Upon receiving the 
       button press signal, it reassigns a value of 1 to the global variable 
       button_push. This will cause one of the elif conditions to run in the 
       while loop of the Finite State Machine in the main program.
       @param IRQ_src The callback function must accept exactly 1 argument, 
       which is the line that triggered the interrupt. In this case, the
       parameter is not interacted with directly by the user.
    '''
    global button_push
    button_push = 1
    
def update_sqw(current_time):
    '''@brief Outputs signal to LED to display a square waveform pattern
       @details This function sends a pulse width modulation signal to LED so
       that it diplays a 50% on duty cycle with a frequency of 1 Hz.
       @param current_time This parameter represents the time elapsed since the
       time of most recent button interrupt.
       @return Returns a brightness percentage according to a square waveform.
    '''
    return 100*(current_time%1.0<0.5)

def update_sin(current_time):
    '''@brief Outputs signal to LED to display a sine waveform pattern
       @details This function sends a pulse width modulation signal to LED so
       that it diplays a sinewave pattern with a frequency of 0.1 Hz.
       @param current_time This parameter represents the time elapsed since the
       time of most recent button interrupt.
       @return Returns a brightness percentage according to a sine waveform.
    '''
    return 100*(0.5 + 0.5*math.sin(math.pi*current_time/5))

def update_stw(current_time):
    '''@brief Outputs signal to LED to display a sawtooth waveform pattern
       @details This function sends a pulse width modulation signal to LED so
       that it diplays a sawtooth pattern with a frequency of 1 Hz.
       @param current_time This parameter represents the time elapsed since the
       time of most recent button interrupt.
       @return Returns a brightness percentage according to a sawtooth waveform.
    '''
    return 100*(current_time%1.0)

if __name__ == '__main__':
    ##  @brief The state the Finite State Machine is about to run
    #   @details State variable is reassigned next value in cycle of FSM after 
    #   each button push and determines which signal LED will display.
    state = 0
    
    ##  @brief The number of iterations of the Finite State Machine.
    #   @details Increments with each new iteration of FSM to track how many
    #   times FSM has run.
    runs = 0
    
    ##  @brief Associates button press with callback function
    #   @details Sets up button interrupt request to call function upon
    #   receiving button press signal
    ButtonInt = pyb.ExtInt(_pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    while True:
        try:                        # If no keyboard interrupt, run FSM
            
            if state == 0:
                # run state 0 code
                print('Welcome! Rules: Press the blue button to cycle through LED patterns')
                state = 1           # Transition to state 1
            
            elif state == 1:
                # run state 1 code: wait for button push to go to square wave
                if button_push == 1:
                    state = 2       # Transition to state 2
                    button_push = 0
                    print('Square Wave pattern selected.')
                    _startTime2 = utime.ticks_ms()
            
            elif state == 2:
                # run state 2 code: square wave
                _stopTime2 = utime.ticks_ms()
                ##  @brief Current time in main program
                #   @details Current time represents the time elapsed since 
                #   last button push for timing purposes in LED pattern.
                current_time = utime.ticks_diff(_stopTime2, _startTime2)/1000
                _t2ch1.pulse_width_percent(update_sqw(current_time))
                if button_push == 1:
                    state = 3       # Transition to state 3
                    button_push = 0
                    print('Sine Wave pattern selected.')
                    _startTime3 = utime.ticks_ms()
                            
            elif state == 3:
                # run state 3 code: sine wave
                _stopTime3 = utime.ticks_ms()
                current_time = utime.ticks_diff(_stopTime3, _startTime3)
                current_time = current_time/1000
                _t2ch1.pulse_width_percent(update_sin(current_time))
                if button_push == 1:
                    state = 4       # Transition to state 4
                    button_push = 0
                    print('Sawtooth pattern selected.')
                    _startTime4 = utime.ticks_ms()
            
            elif state == 4:
                # run state 4 code: sawtooth wave
                _stopTime4 = utime.ticks_ms()
                current_time = utime.ticks_diff(_stopTime4, _startTime4)
                current_time = current_time/1000
                _t2ch1.pulse_width_percent(update_stw(current_time))
                if button_push == 1:
                    state = 2       # Transition to state 2
                    button_push = 0
                    print('Square Wave pattern selected.')
                    _startTime2 = utime.ticks_ms()
                
            time.sleep(0.01)        # Slow down FSM so we can read output
            
            runs += 1               # Increment run counter to track number
                                    # of FSM runs
            
        except KeyboardInterrupt:   # If keyboard interrupt, exit loop
            break
        
    print('Program Terminated')